
public class App {

	public static void main(String[] args) {

		int result = 1 + 2;
		System.out.println("1+2 = " + result);

		int previousResult = result;
		result = result - 1;
		System.out.println(result);

		previousResult = result;

		result = result * 10;
		System.out.println(result);

		result = result / 5;
		System.out.println(result);

		previousResult = result;
		result = result % 3;
		System.out.println(result);

		previousResult = result;
		result = result + 1;
		result++;
		result--;

		result += 2;
		result *= 10;
		result -= 10;
		result /= 10;

		boolean isAlien = false;
		if (isAlien == true)
			System.out.println("It is not an alien!");

		int topScore = 99;
		if (topScore < 100)
			System.out.println("top score");

		int secondTopScore = 81;
		if ((topScore > secondTopScore) && (topScore < 100))
			System.out.println("Greater than second top score and less than a hundred");

		if ((topScore > 90) || (secondTopScore <= 90))
			System.out.println("one of these is true");
		
		int newValue = 50;
		if (newValue == 50)
			System.out.println("This is true");
		
		boolean isCar = false;
		if(isCar)
			System.out.println("This is not supposed to happen");

		
		isCar = true;
		boolean wasCar = isCar ? true : false;
		if(wasCar)
		System.out.println("wasCar true");
		
		double myDouble1 = 20;
		double myDouble2 = 80;
		
		double totalValue = (myDouble1 + myDouble2) * 25;
		System.out.println(totalValue);
		double myDivide = totalValue % 40;
		System.out.println(myDivide);
		
		if(myDivide <= 20) {
			
			System.out.println("Total was over the limit");
			
		}
		
		
		int a = 3;
		int b = 6;
		int minVal = (a < b) ? a : b;
		System.out.println(minVal);
		
	}

}
